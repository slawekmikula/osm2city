# road texture coordinates in osm2city-data/tex/roads.png
TRACK = (0/8., 1/8.)
ROAD_1 = (1/8., 2/8.)
ROAD_2 = (2/8., 3/8.)
BRIDGE_1 = (3/8., 4/8.)
EMBANKMENT_1 = (4/8., 5/8.)
ROAD_3 = (5/8., 6/8.)
EMBANKMENT_2 = (6/8., 7/8.)
TRAMWAY = (7/8., 8/8.)

BOTTOM = (4/8.-0.05, 4/8.)
