
Welcome to osm2city's documentation!
====================================
While the article on FlightGear's Wiki_ presents in general terms what ``osm2city`` is and what features it has, this manual goes into depth with many aspects of generating scenery objects based on OpenStreetMap_ (OSM) data and deploying/using them in FlightGear_.


.. _FlightGear: http://www.flightgear.org/
.. _Wiki: http://wiki.flightgear.org/Osm2city.py
.. _OpenStreetMap: http://www.osm.org/


Before you generate your own sceneries, you might want to get familiar with the output of ``osm2city`` by first deploying some of the downloadable osm2city sceneries and have a look at chapter :ref:`Using Generated Scenery <chapter-using-label>`. The following list is not exhaustive and more sceneries are typically announced in the Sceneries_ part of the FlightGear Forums:

* `Areas populated with osm2city scenery <http://wiki.flightgear.org/Areas_populated_with_osm2city_scenery>`_
* `LOWI city buildings <http://forum.flightgear.org/viewtopic.php?f=5&t=19625>`_
* `EDDC city models <http://forum.flightgear.org/viewtopic.php?f=5&t=18851#p174940>`_
* `OMDB & OMDW <http://forum.flightgear.org/viewtopic.php?f=5&t=28218>`_


.. _Sceneries: http://forum.flightgear.org/viewforum.php?f=5

**Contents:**

.. toctree::
   :maxdepth: 2
   
   installation
   preparation
   generation
   using
   parameters
   appendix


**Indices and tables:**

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

